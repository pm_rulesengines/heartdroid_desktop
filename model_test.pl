
xtype [	name : xtype_0 ,
	base : numeric ,
	domain : [34.0 to 43.0] ] .
xtype [	name : xtype_1 ,
	base : numeric ,
	domain : [31.0 to 40.0] ] .
xtype [	name : xtype_2 ,
	base : symbolic ,
	domain : ['c'/1, 'd'/2, 'e'/3, 'f'/4, 'g'/5, 'h'/6, 'i'/7, 'j'/8, 'k'/9, 'l'/10] ,
	ordered : yes ] .
xtype [	name : xtype_3 ,
	base : symbolic ,
	domain : ['g'/1, 'h'/2, 'i'/3, 'j'/4, 'k'/5, 'l'/6, 'm'/7, 'n'/8, 'o'/9, 'p'/10] ,
	ordered : yes ] .
xtype [	name : xtype_4 ,
	base : symbolic ,
	domain : ['k'/1, 'l'/2, 'm'/3, 'n'/4, 'o'/5, 'p'/6, 'q'/7, 'r'/8, 's'/9, 't'/10] ,
	ordered : yes ] .
xattr [	name : xattr_0 ,
	abbrev : xattr_0 ,
	class : simple ,
	type : xtype_2 ] .
xattr [	name : xattr_1 ,
	abbrev : xattr_1 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_2 ,
	abbrev : xattr_2 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_3 ,
	abbrev : xattr_3 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_4 ,
	abbrev : xattr_4 ,
	class : simple ,
	type : xtype_4 ] .
xattr [	name : xattr_5 ,
	abbrev : xattr_5 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_6 ,
	abbrev : xattr_6 ,
	class : simple ,
	type : xtype_2 ] .
xattr [	name : xattr_7 ,
	abbrev : xattr_7 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_8 ,
	abbrev : xattr_8 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_9 ,
	abbrev : xattr_9 ,
	class : simple ,
	type : xtype_0 ] .
xattr [	name : xattr_10 ,
	abbrev : xattr_10 ,
	class : simple ,
	type : xtype_1 ] .
xattr [	name : xattr_11 ,
	abbrev : xattr_11 ,
	class : simple ,
	type : xtype_1 ] .
xattr [	name : xattr_12 ,
	abbrev : xattr_12 ,
	class : simple ,
	type : xtype_1 ] .
xattr [	name : xattr_13 ,
	abbrev : xattr_13 ,
	class : simple ,
	type : xtype_3 ] .
xattr [	name : xattr_14 ,
	abbrev : xattr_14 ,
	class : simple ,
	type : xtype_3 ] .
xschm xschm_0 :
 [xattr_0, xattr_1, xattr_2, xattr_3] ==> [xattr_4, xattr_5, xattr_6, xattr_7].
xschm xschm_1 :
 [xattr_4, xattr_5, xattr_6, xattr_7] ==> [xattr_8, xattr_9, xattr_10].
xschm xschm_2 :
 [xattr_8, xattr_9, xattr_10] ==> [xattr_11, xattr_12, xattr_13, xattr_14].
xrule xschm_0/0 :
[
xattr_0 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_1 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_2 in [34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_3 in [34.0, 35.0, 36.0] ]
==>
[
xattr_4 set 'q' , 
xattr_5 set 38.0 , 
xattr_6 set 'e' , 
xattr_7 set 'g' ].

xrule xschm_0/1 :
[
xattr_0 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_1 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_2 in [34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_3 in [37.0, 38.0, 39.0, 40.0, 41.0, 42.0, 43.0] ]
==>
[
xattr_4 set 's' , 
xattr_5 set 40.0 , 
xattr_6 set 'k' , 
xattr_7 set 'o' ].

xrule xschm_0/2 :
[
xattr_0 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_1 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_2 in [41.0, 42.0, 43.0] , 
xattr_3 in [34.0, 35.0, 36.0] ]
==>
[
xattr_4 set 's' , 
xattr_5 set 43.0 , 
xattr_6 set 'g' , 
xattr_7 set 'p' ].

xrule xschm_0/3 :
[
xattr_0 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_1 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'] , 
xattr_2 in [41.0, 42.0, 43.0] , 
xattr_3 in [37.0, 38.0, 39.0, 40.0, 41.0, 42.0, 43.0] ]
==>
[
xattr_4 set 'k' , 
xattr_5 set 40.0 , 
xattr_6 set 'l' , 
xattr_7 set 'j' ].

xrule xschm_0/4 :
[
xattr_0 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_1 eq 'p' , 
xattr_2 in [34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_3 in [34.0, 35.0, 36.0] ]
==>
[
xattr_4 set 'o' , 
xattr_5 set 35.0 , 
xattr_6 set 'i' , 
xattr_7 set 'k' ].

xrule xschm_0/5 :
[
xattr_0 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_1 eq 'p' , 
xattr_2 in [34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_3 in [37.0, 38.0, 39.0, 40.0, 41.0, 42.0, 43.0] ]
==>
[
xattr_4 set 'r' , 
xattr_5 set 34.0 , 
xattr_6 set 'e' , 
xattr_7 set 'l' ].

xrule xschm_0/6 :
[
xattr_0 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_1 eq 'p' , 
xattr_2 in [41.0, 42.0, 43.0] , 
xattr_3 in [34.0, 35.0, 36.0] ]
==>
[
xattr_4 set 'o' , 
xattr_5 set 37.0 , 
xattr_6 set 'j' , 
xattr_7 set 'h' ].

xrule xschm_0/7 :
[
xattr_0 in ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'] , 
xattr_1 eq 'p' , 
xattr_2 in [41.0, 42.0, 43.0] , 
xattr_3 in [37.0, 38.0, 39.0, 40.0, 41.0, 42.0, 43.0] ]
==>
[
xattr_4 set 'l' , 
xattr_5 set 40.0 , 
xattr_6 set 'l' , 
xattr_7 set 'g' ].

xrule xschm_1/0 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 42.0 , 
xattr_10 set 39.0 ].

xrule xschm_1/1 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'p' , 
xattr_9 set 36.0 , 
xattr_10 set 35.0 ].

xrule xschm_1/2 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'g' , 
xattr_9 set 35.0 , 
xattr_10 set 37.0 ].

xrule xschm_1/3 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 43.0 , 
xattr_10 set 32.0 ].

xrule xschm_1/4 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'i' , 
xattr_9 set 35.0 , 
xattr_10 set 33.0 ].

xrule xschm_1/5 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'p' , 
xattr_9 set 42.0 , 
xattr_10 set 37.0 ].

xrule xschm_1/6 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'g' , 
xattr_9 set 38.0 , 
xattr_10 set 39.0 ].

xrule xschm_1/7 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'o' , 
xattr_9 set 36.0 , 
xattr_10 set 40.0 ].

xrule xschm_1/8 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'g' , 
xattr_9 set 39.0 , 
xattr_10 set 32.0 ].

xrule xschm_1/9 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'l' , 
xattr_9 set 36.0 , 
xattr_10 set 34.0 ].

xrule xschm_1/10 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'i' , 
xattr_9 set 35.0 , 
xattr_10 set 33.0 ].

xrule xschm_1/11 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 41.0 , 
xattr_10 set 38.0 ].

xrule xschm_1/12 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'j' , 
xattr_9 set 42.0 , 
xattr_10 set 35.0 ].

xrule xschm_1/13 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'm' , 
xattr_9 set 39.0 , 
xattr_10 set 32.0 ].

xrule xschm_1/14 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'l' , 
xattr_9 set 43.0 , 
xattr_10 set 31.0 ].

xrule xschm_1/15 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'h' , 
xattr_9 set 41.0 , 
xattr_10 set 40.0 ].

xrule xschm_1/16 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 42.0 , 
xattr_10 set 34.0 ].

xrule xschm_1/17 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'p' , 
xattr_9 set 40.0 , 
xattr_10 set 33.0 ].

xrule xschm_1/18 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'm' , 
xattr_9 set 42.0 , 
xattr_10 set 39.0 ].

xrule xschm_1/19 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'h' , 
xattr_9 set 40.0 , 
xattr_10 set 38.0 ].

xrule xschm_1/20 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'k' , 
xattr_9 set 37.0 , 
xattr_10 set 38.0 ].

xrule xschm_1/21 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'o' , 
xattr_9 set 36.0 , 
xattr_10 set 40.0 ].

xrule xschm_1/22 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'l' , 
xattr_9 set 34.0 , 
xattr_10 set 32.0 ].

xrule xschm_1/23 :
[
xattr_4 in ['k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 39.0 , 
xattr_10 set 40.0 ].

xrule xschm_1/24 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'o' , 
xattr_9 set 40.0 , 
xattr_10 set 39.0 ].

xrule xschm_1/25 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'h' , 
xattr_9 set 34.0 , 
xattr_10 set 39.0 ].

xrule xschm_1/26 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'h' , 
xattr_9 set 35.0 , 
xattr_10 set 32.0 ].

xrule xschm_1/27 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'k' , 
xattr_9 set 43.0 , 
xattr_10 set 33.0 ].

xrule xschm_1/28 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'p' , 
xattr_9 set 34.0 , 
xattr_10 set 37.0 ].

xrule xschm_1/29 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [34.0, 35.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 34.0 , 
xattr_10 set 36.0 ].

xrule xschm_1/30 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'm' , 
xattr_9 set 40.0 , 
xattr_10 set 38.0 ].

xrule xschm_1/31 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'k' , 
xattr_9 set 43.0 , 
xattr_10 set 37.0 ].

xrule xschm_1/32 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'k' , 
xattr_9 set 43.0 , 
xattr_10 set 37.0 ].

xrule xschm_1/33 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'j' , 
xattr_9 set 42.0 , 
xattr_10 set 38.0 ].

xrule xschm_1/34 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'h' , 
xattr_9 set 36.0 , 
xattr_10 set 36.0 ].

xrule xschm_1/35 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [36.0, 37.0, 38.0, 39.0, 40.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 39.0 , 
xattr_10 set 38.0 ].

xrule xschm_1/36 :
[
xattr_4 in ['s', 't'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'i' , 
xattr_9 set 40.0 , 
xattr_10 set 36.0 ].

xrule xschm_1/37 :
[
xattr_4 in ['s', 't'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'p' , 
xattr_9 set 37.0 , 
xattr_10 set 32.0 ].

xrule xschm_1/38 :
[
xattr_4 in ['s', 't'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'l' , 
xattr_9 set 38.0 , 
xattr_10 set 35.0 ].

xrule xschm_1/39 :
[
xattr_4 in ['s', 't'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'o' , 
xattr_9 set 42.0 , 
xattr_10 set 40.0 ].

xrule xschm_1/40 :
[
xattr_4 in ['s', 't'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'p' , 
xattr_9 set 43.0 , 
xattr_10 set 39.0 ].

xrule xschm_1/41 :
[
xattr_4 in ['s', 't'] , 
xattr_5 eq 41.0 , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 37.0 , 
xattr_10 set 40.0 ].

xrule xschm_1/42 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'l' , 
xattr_9 set 38.0 , 
xattr_10 set 33.0 ].

xrule xschm_1/43 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'j' , 
xattr_9 set 38.0 , 
xattr_10 set 36.0 ].

xrule xschm_1/44 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['c', 'd', 'e', 'f', 'g'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'p' , 
xattr_9 set 34.0 , 
xattr_10 set 32.0 ].

xrule xschm_1/45 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 in ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'] ]
==>
[
xattr_8 set 'k' , 
xattr_9 set 42.0 , 
xattr_10 set 38.0 ].

xrule xschm_1/46 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'o' ]
==>
[
xattr_8 set 'm' , 
xattr_9 set 40.0 , 
xattr_10 set 31.0 ].

xrule xschm_1/47 :
[
xattr_4 in ['s', 't'] , 
xattr_5 in [42.0, 43.0] , 
xattr_6 in ['h', 'i', 'j', 'k', 'l'] , 
xattr_7 eq 'p' ]
==>
[
xattr_8 set 'n' , 
xattr_9 set 38.0 , 
xattr_10 set 39.0 ].

xrule xschm_2/0 :
[
xattr_8 in ['g', 'h'] , 
xattr_9 in [34.0, 35.0, 36.0, 37.0] , 
xattr_10 in [31.0, 32.0, 33.0, 34.0] ]
==>
[
xattr_11 set 39.0 , 
xattr_12 set 35.0 , 
xattr_13 set 'h' , 
xattr_14 set 'l' ].

xrule xschm_2/1 :
[
xattr_8 in ['g', 'h'] , 
xattr_9 in [34.0, 35.0, 36.0, 37.0] , 
xattr_10 in [35.0, 36.0, 37.0, 38.0, 39.0, 40.0] ]
==>
[
xattr_11 set 40.0 , 
xattr_12 set 33.0 , 
xattr_13 set 'j' , 
xattr_14 set 'g' ].

xrule xschm_2/2 :
[
xattr_8 in ['g', 'h'] , 
xattr_9 in [38.0, 39.0, 40.0, 41.0, 42.0, 43.0] , 
xattr_10 in [31.0, 32.0, 33.0, 34.0] ]
==>
[
xattr_11 set 33.0 , 
xattr_12 set 31.0 , 
xattr_13 set 'j' , 
xattr_14 set 'i' ].

xrule xschm_2/3 :
[
xattr_8 in ['g', 'h'] , 
xattr_9 in [38.0, 39.0, 40.0, 41.0, 42.0, 43.0] , 
xattr_10 in [35.0, 36.0, 37.0, 38.0, 39.0, 40.0] ]
==>
[
xattr_11 set 40.0 , 
xattr_12 set 39.0 , 
xattr_13 set 'j' , 
xattr_14 set 'k' ].

xrule xschm_2/4 :
[
xattr_8 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_9 in [34.0, 35.0, 36.0, 37.0] , 
xattr_10 in [31.0, 32.0, 33.0, 34.0] ]
==>
[
xattr_11 set 33.0 , 
xattr_12 set 33.0 , 
xattr_13 set 'k' , 
xattr_14 set 'm' ].

xrule xschm_2/5 :
[
xattr_8 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_9 in [34.0, 35.0, 36.0, 37.0] , 
xattr_10 in [35.0, 36.0, 37.0, 38.0, 39.0, 40.0] ]
==>
[
xattr_11 set 36.0 , 
xattr_12 set 31.0 , 
xattr_13 set 'k' , 
xattr_14 set 'o' ].

xrule xschm_2/6 :
[
xattr_8 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_9 in [38.0, 39.0, 40.0, 41.0, 42.0, 43.0] , 
xattr_10 in [31.0, 32.0, 33.0, 34.0] ]
==>
[
xattr_11 set 39.0 , 
xattr_12 set 31.0 , 
xattr_13 set 'n' , 
xattr_14 set 'n' ].

xrule xschm_2/7 :
[
xattr_8 in ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'] , 
xattr_9 in [38.0, 39.0, 40.0, 41.0, 42.0, 43.0] , 
xattr_10 in [35.0, 36.0, 37.0, 38.0, 39.0, 40.0] ]
==>
[
xattr_11 set 38.0 , 
xattr_12 set 32.0 , 
xattr_13 set 'l' , 
xattr_14 set 'h' ].
